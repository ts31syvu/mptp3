# DEMO
## QR-Code
<img src="./files/sylvester.jpg" width="500"/>
<img src="./files/mensa.jpg" width="500"/>

# Extract data from qr code
```SH
python ./extract.py ./files/sylvester.jpg >> ./files/locations.txt
python ./extract.py ./files/mensa.jpg >> ./files/locations.txt
```

## Create a examplary Check-In
```SH
python checkin.py "$(head -n 1 ./files/locations.txt)" >> ./files/checkins.txt
```

## Create submission/infection for one Check-In
```SH
# Create submission payload
python generate_submission.py "$(head -n 1 ./files/locations.txt)" > ./files/submission.pb
# Send to submission service
sh submit.sh ./files/submission.pb
# Restart distribution service so that data is in object store
docker restart cwa-server-distribution-1 && docker wait cwa-server-distribution-1
```

## Check if location has infections
```SH
cat ./files/locations.txt | while read line; do python hasinfections.py "$line" 2>/dev/null; done
```
