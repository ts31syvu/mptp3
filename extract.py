import sys

from pathlib import Path

from pyzbar import pyzbar
from PIL import Image


def die(msg: str):
    sys.stderr.write(msg)
    if not msg.endswith("\n"):
        sys.stderr.write("\n")
    sys.exit(1)

def main():
    if len(sys.argv) < 2:
        die(f"Usage: {sys.argv[0]} qrcode.png")

    ipath: Path = Path(sys.argv[1])
    if not ipath.exists():
        die("The provided qrcode image does not exist at the provided path")

    [output] = pyzbar.decode(Image.open(ipath))
    url = output.data.decode()
    print(url)

if __name__ == "__main__":
    main()
