FROM alpine:latest
RUN apk add protoc py3-pip zbar

ADD ./requirements.txt /requirements.txt
RUN python3 -m pip install -r /requirements.txt
RUN rm /requirements.txt

RUN ln -s /usr/bin/python3 /usr/bin/python
