import base64
import string
import sys
import random

from hashlib import sha256
from argparse import ArgumentParser
from pathlib import Path

from models.qr_pb2 import QRCodePayload
from models.checkin_pb2 import CheckIn

def die(msg: str):
    sys.stderr.write(msg)
    if not msg.endswith("\n"):
        sys.stderr.write("\n")
    sys.exit(1)

def main():
    if len(sys.argv) != 2:
        die(f"Usage {Path(sys.argv[0]).name} location-uri")

    url = sys.argv[1]

    _, data = url.split("#", maxsplit=1)
    data = data.strip() # remove unintentional whitespaces
    data = data + ("="*(len(data)%3)) # add padding chars
    data = data.encode("ascii")
    data = base64.urlsafe_b64decode(data)

    # location id see TraceLocation.kt
    locationId = sha256("CWA-GUID".encode() + data).digest()

    # deserialized qrcode
    qrcode = QRCodePayload()
    qrcode.ParseFromString(data)
    event_start = qrcode.locationData.startTimestamp
    event_end = qrcode.locationData.endTimestamp

    # calculate random stay
    MIN_STAY = 60
    offset = random.randint(0, (event_end - MIN_STAY) - event_start)
    stay = random.randint(0, event_end - (event_start+offset))

    # checkin must have the following constraints
    # https://github.com/corona-warn-app/cwa-server/blob/54ef6830b61ccb02751a3c333d7a02eea1933942/services/submission/src/main/java/app/coronawarn/server/services/submission/checkins/EventCheckinDataValidator.java
    checkin = CheckIn()
    checkin.locationId = locationId
    checkin.startIntervalNumber = event_start + offset
    checkin.endIntervalNumber = event_start + offset + stay
    checkin.transmissionRiskLevel = random.randint(1, 8)
    sys.stderr.write(str(checkin) + "\n")
    sys.stdout.write(base64.b64encode(checkin.SerializeToString()).decode() + "\n")

if __name__ == "__main__":
    main()
