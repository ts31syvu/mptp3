import io
import sys
import json
import base64
import zipfile

from datetime import datetime
from pathlib import Path
from typing import Generator, Tuple
from hashlib import sha256

import boto3

from models.qr_pb2 import TraceLocation, QRCodePayload
from models.checkin_pb2 import CheckInProtectedReport
# https://github.com/google/exposure-notifications-server/blob/HEAD/internal/pb/export/export.proto
from models.infections_pb2 import TemporaryExposureKeyExport, SignatureInfo
from models.tracewarningpackage_pb2 import TraceWarningPackage

session = boto3.session.Session()
s3_client = session.client(
    service_name="s3",
    aws_access_key_id="accessKey1",
    aws_secret_access_key="verySecretKey1",
    endpoint_url="http://localhost:8003",
)

def die(msg: str):
    sys.stderr.write(msg)
    if not msg.endswith("\n"):
        sys.stderr.write("\n")
    sys.exit(1)

def log(*args):
    for arg in args:
        sys.stderr.write(str(arg))
        sys.stderr.write(" ")
    sys.stderr.write("\n")

def locationid_from_uri(uri: str) -> str:
    # extract the serialized data from the url
    _, data = uri.split("#", maxsplit=1)
    data = data.strip() # remove unintentional whitespaces
    data = data + ("="*(len(data)%3)) # add padding chars
    data = data.encode("ascii")
    data = base64.urlsafe_b64decode(data)

    # location id see TraceLocation.kt
    location_id = sha256("CWA-GUID".encode() + data).digest()

    return location_id

def location_from_uri(uri: str) -> TraceLocation:

    _, data = uri.split("#", maxsplit=1)
    data = data.strip() # remove unintentional whitespaces
    data = data + ("="*(len(data)%3)) # add padding chars
    data = data.encode("ascii")
    data = base64.urlsafe_b64decode(data)

    # deserialized qrcode
    qrcode = QRCodePayload()
    qrcode.ParseFromString(data)

    return qrcode.locationData

def get(key: str) -> bytes:
    try:
        resp = s3_client.get_object(Bucket="cwa", Key=key)["Body"].read()
        log(f"OK    | {key}")
        return resp
    except Exception as e:
        log(f"ERROR | {key} | {e}")
    return None

def get_infections() -> Generator[bytes, None, None]:
    versions = lambda: json.loads(get("version"))
    countries = lambda v: json.loads(get(f"version/{v}/diagnosis-keys/country"))
    dates = lambda v, c: json.loads(get(f"version/{v}/diagnosis-keys/country/{c}/date"))
    day_data = lambda v, c, d: get(f"version/{v}/diagnosis-keys/country/{c}/date/{d}")

    for version in versions():
        for country in countries(version):
            for date in dates(version, country):
                f = zipfile.ZipFile(io.BytesIO(day_data(version, country, date)))
                if "export.bin" not in f.namelist() or "export.sig" not in f.namelist():
                    sys.stderr.write(f"Bad export {version}/{date}\n")

                # 16 byte offset: see https://github.com/google/exposure-notifications-server/blob/HEAD/internal/pb/export/export.proto
                data = TemporaryExposureKeyExport()
                data.ParseFromString(f.read("export.bin")[16:])

                for key in data.keys:
                    yield key

    return []

def get_infected_location_id_hashes():
    versions = lambda: json.loads(get("version/index-v2"))
    countries = lambda v: json.loads(get(f"version/{v}/twp/country"))
    hours = lambda v, c: json.loads(get(f"version/{v}/twp/country/{c}/hour") or "{}")
    hour_data = lambda v, c, h: get(f"version/{v}/twp/country/{c}/hour/{h}")

    for version in versions():
        for country in countries(version):
            for hour in hours(version, country).values():
                if hour is None: continue

                f = zipfile.ZipFile(io.BytesIO(hour_data(version, country, hour)))
                if "export.bin" not in f.namelist() or "export.sig" not in f.namelist():
                    sys.stderr.write(f"Bad export !!\n")

                twp = TraceWarningPackage()
                twp.ParseFromString(f.read("export.bin"))
                for report in twp.checkInProtectedReports:
                    yield report.locationIdHash

def main():
    if len(sys.argv) != 2:
        die(f"Usage {Path(sys.argv[0]).name} location-uri")

    lhash = sha256(locationid_from_uri(sys.argv[1])).digest()
    location = location_from_uri(sys.argv[1])

    for other_lhash in get_infected_location_id_hashes():
        if lhash == other_lhash:
            print("--- INFECTION ---")
            print("Adress", location.address)
            print("Description", location.description)
            print("Start", datetime.fromtimestamp(location.startTimestamp))
            print("End", datetime.fromtimestamp(location.endTimestamp))
            print("-----------------")

if __name__ == "__main__":
    main()
