import random
import time
import os
import sys
import base64
import hmac
import urllib3

from typing import List, Tuple
from datetime import datetime, timedelta
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad,unpad
from hashlib import sha256
from pathlib import Path

from models.submission_pb2 import SubmissionPayload
from models.infections_pb2 import TemporaryExposureKey
from models.checkin_pb2 import CheckIn, CheckInRecord, CheckInProtectedReport

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def die(msg: str):
    sys.stderr.write(msg)
    if not msg.endswith("\n"):
        sys.stderr.write("\n")
    sys.exit(1)

def log(*args):
    for arg in args:
        sys.stderr.write(str(arg))
        sys.stderr.write(" ")
    sys.stderr.write("\n")

# https://stackoverflow.com/questions/66862391/aes-128-cbc-encryption-in-python
def encrypt(data: bytes, key: bytes, iv: bytes):
    data = pad(data, 16)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return base64.b64encode(cipher.encrypt(data))

def generate_exposure_keys(num: int,
                           today: datetime,
                           risk: int,
                           rolling_period: int,
                           report_type: int,
                           days_since_onset_of_symptoms: int
                          ) -> List[TemporaryExposureKey]:
    result: List[TemporaryExposureKey] = []

    for i in range(num):
        data = random.randbytes(16)

        log(today, int((today.timestamp()/600) + (rolling_period * i)))

        tmpkey = TemporaryExposureKey()
        tmpkey.key_data = data
        tmpkey.transmission_risk_level = risk
        tmpkey.rolling_start_interval_number = int((today.timestamp()/600) + (rolling_period * i))
        tmpkey.rolling_period = rolling_period
        tmpkey.report_type = report_type
        tmpkey.days_since_onset_of_symptoms = days_since_onset_of_symptoms

        result.append(tmpkey)

    return result

def generate_checkins_for_location(num: int,
                                   today: datetime,
                                   location_id: bytes,
                                   risk: int,
                                  ) -> List[CheckIn]:
    result: List[CheckIn] = []
    for i in range(num):
        start = today + timedelta(days = i, hours=random.randint(0, 22), minutes=random.randint(0, 50))
        end = start + timedelta(minutes=random.randint(5, 50)) # 5-50 min stay

        checkin = CheckIn()
        checkin.locationId = location_id
        checkin.startIntervalNumber = int(start.timestamp())
        checkin.endIntervalNumber = int(end.timestamp())
        checkin.transmissionRiskLevel = risk

        log("Checkin  ", checkin)
        result.append(checkin)

    return result

def generate_prot_checkin_reports_for_location(num: int,
                                               today: datetime,
                                               location_id: bytes,
                                              ) -> List[CheckInProtectedReport]:
    result: List[CheckInProtectedReport] = []

    for i in range(num):
        start: datetime = today + timedelta(i, 0, 0, 0, 0, random.randint(0, 23), 0)
        duration = random.randint(60, 60*50) # 1min to 50min

        # see CheckInCryptography.kt
        iv = os.urandom(16)
        enckey = sha256(bytes.fromhex("4357412d454e4352595054494f4e2d4b4559") + location_id).digest()

        checkin_record = CheckInRecord()
        checkin_record.startIntervalNumber = int(start.timestamp())
        checkin_record.period = duration
        checkin_record.transmissionRiskLevel = 2

        encrpyed_record = random.randbytes(16)
        #encrpyed_record = encrypt(checkin_record.SerializeToString(), enckey, iv)

        mackey = sha256(bytes.fromhex("4357412d4d41432d4b4559") + location_id).digest()
        mac = hmac.new(mackey, iv + encrpyed_record, sha256).digest()

        report = CheckInProtectedReport()
        report.locationIdHash = sha256(location_id).digest() # hashed twice
        report.iv = iv
        report.encryptedCheckInRecord = encrpyed_record
        report.mac = mac

        result.append(report)

    return result

def locationid_from_uri(uri: str) -> str:
    # extract the serialized data from the url
    _, data = uri.split("#", maxsplit=1)
    data = data.strip() # remove unintentional whitespaces
    data = data + ("="*(len(data)%3)) # add padding chars
    data = data.encode("ascii")
    data = base64.urlsafe_b64decode(data)

    # location id see TraceLocation.kt
    location_id = sha256("CWA-GUID".encode() + data).digest()

    return location_id

def build_submission_payload(keys: List[TemporaryExposureKey],
                             checkins: List[CheckIn],
                             prot_checkin_reports: List[CheckInProtectedReport],
                             req_padding: bytes,
                             visited_countries: List[str],
                             origin_country: str,
                             consent_to_fed: bool
                            ) -> SubmissionPayload:
    payload = SubmissionPayload()
    payload.origin = origin_country
    payload.consentToFederation = consent_to_fed
    payload.requestPadding = req_padding

    for c in visited_countries:
        payload.visitedCountries.append(c)
    for c in checkins:
        payload.checkIns.append(c)
    for c in prot_checkin_reports:
        payload.checkInProtectedReports.append(c)
    for k in keys:
        payload.keys.append(k)

    return payload

def main():
    if len(sys.argv) != 2:
        die(f"Usage {Path(sys.argv[0]).name} location-uri")

    location_id = locationid_from_uri(sys.argv[1])

    NUM_KEYS = 10 #min 7 max 100
    NUM_CHECKINS = 2
    TRANSMISSION_RISK_LEVEL = 6
    ROLLING_PERIOD = 144
    REPORT_TYPE = 1 # CONFIRMED TEST
    DAYS_SINCE_ONSET_OF_SYMPTOMS = 0
    REQUEST_PADDING = bytes(0 for _ in range(100))
    VISITED_COUNTRIES = ["DE"]
    ORIGIN_COUNTRY = "DE"
    CONSENT_TO_FEDERATION = True

    now = datetime.fromtimestamp(time.time())
    today_midnight = datetime(now.year, now.month, now.day)
    today_midnight_minus_num_keys = today_midnight - timedelta(days=NUM_KEYS)

    temp_exp_keys = generate_exposure_keys(
        NUM_KEYS,
        today_midnight_minus_num_keys,
        TRANSMISSION_RISK_LEVEL,
        ROLLING_PERIOD,
        REPORT_TYPE,
        DAYS_SINCE_ONSET_OF_SYMPTOMS
    )

    checkins = generate_checkins_for_location(
        NUM_CHECKINS,
        today_midnight_minus_num_keys,
        location_id,
        TRANSMISSION_RISK_LEVEL,
    )

    checkin_prot_reports = generate_prot_checkin_reports_for_location(
        NUM_CHECKINS,
        today_midnight_minus_num_keys,
        location_id
    )

    payload = build_submission_payload(
        temp_exp_keys,
        checkins,
        checkin_prot_reports,
        REQUEST_PADDING,
        VISITED_COUNTRIES,
        ORIGIN_COUNTRY,
        CONSENT_TO_FEDERATION,
    )

    sys.stdout.buffer.write(payload.SerializeToString())

if __name__ == "__main__":
    main()
