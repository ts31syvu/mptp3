## Dependencies
```SH
sudo apt install -y libzbar-dev

python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

## Clone repo
```SH
# assumes that everything is run from this repo: cd this_repo_dir
git clone https://github.com/corona-warn-app/cwa-server.git ~/StudioProjects/cwa-server
git apply -C "~/StudioProjects/cwa-server" $(realpath ./cwa-server.patch)
docker-compose -f ~/StudioProjects/cwa-server/docker-compose up
```

## Build protos in docker
```SH
docker build -t mptp .

# protos
docker run -it --rm -u $UID:$GID -v $(pwd):/stuff -w /stuff mptp protoc -I=./protos/ --python_out=./models ./protos/*.proto

# update import path in:
# submission_pb2.py
    #-> import models.infections_pb2 as infections__pb2
    #-> import models.checkin_pb2 as checkin__pb2
# tracewarningpackage_pb2.py
    #-> import models.checkin_pb2 as checkin__pb2
```
https://docs.google.com/presentation/d/1AOT3XnorYj0DmgS0SuDX5ZL6wOnYaKluMxWXzsAy0eg/edit?usp=sharing
