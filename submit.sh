#!/bin/bash
[ -z "$1" ] || ! [ -f "$1" ] && { echo "Usage: $(basename $0) submission-file.pb" >&2; exit 1; }

curl -v --location -k --request POST 'https://localhost:8000/version/v1/diagnosis-keys' \
--header 'CWA-Authorization: edc07f08-a1aa-11ea-bb37-0242ac130002' \
--header 'CWA-Fake: 0' \
--header 'Content-Type: application/x-protobuf' \
--header 'Accept: application/json' \
--data-binary "@$(realpath $1)"
